import React , { Component} from 'react';
import { Header, Icon, Divider, Container } from 'semantic-ui-react';
import TablaLibros from './componentes/TablaLibros';

class App extends Component {
  render(){
    return (
      <div className="App">
        <Header as='h2' icon textAlign = 'center'>
          <Icon circular name = 'book' />
          <Header.Content> Biblioteca </Header.Content>
        </Header>
        <Divider/>
        <Container textAlign = "center">
          <TablaLibros/>
        </Container>
        
      </div>
    );
  }
}

export default App;
