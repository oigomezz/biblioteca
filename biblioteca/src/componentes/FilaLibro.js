import React, { Component } from 'react';
import { Table } from 'semantic-ui-react';

class  FilaLibro extends Component {
   
    render(){
        const { libro } = this.props;
        return (
            <Table.Row>
                <Table.Cell> {libro.title} </Table.Cell>
                <Table.Cell> {libro.author} </Table.Cell>
                <Table.Cell> {libro.content_short} </Table.Cell>
            </Table.Row>
        );
    }
};

export default FilaLibro;
