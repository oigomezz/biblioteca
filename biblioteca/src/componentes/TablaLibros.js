import React, { Component } from 'react';
import { Table } from 'semantic-ui-react';
import FilaLibro from './FilaLibro';

class  TablaLibros extends Component {

    constructor() {
        super();

        this.state = { 
            libros: [],
            isFetch: true,
        };
    }

    componentDidMount() {
        fetch('https://www.etnassoft.com/api/v1/get/?category=libros_programacion&criteria=most_viewed')
        .then((response) => response.json())
        .then((libros) => { this.setState({ libros: libros, isFetch: false})})
    }

    render(){
        if (this.state.isFetch){
            return 'Cargando ...'
        }

        return (
            <Table fixed>
                <Table.Header>
                <Table.Row>
                    <Table.HeaderCell>Titulo</Table.HeaderCell>
                    <Table.HeaderCell>Autor</Table.HeaderCell>
                    <Table.HeaderCell>Descripcion</Table.HeaderCell>
                </Table.Row>
                </Table.Header>

                <Table.Body>
                {this.state.libros.map( libro => (
                    <FilaLibro libro = {libro}/>
                ))}
                </Table.Body>
            </Table>
        );
    }
};

export default TablaLibros;